set background=dark
hi clear
syntax reset
let g:colors_name = 'substratum'

hi None           ctermfg=NONE  ctermbg=NONE  cterm=NONE
hi Ignore         ctermfg=00    ctermbg=NONE  cterm=NONE
hi Normal         ctermfg=07    ctermbg=NONE  cterm=NONE
hi Comment        ctermfg=08    ctermbg=NONE  cterm=NONE
hi Constant       ctermfg=05    ctermbg=NONE  cterm=NONE
hi Error          ctermfg=01    ctermbg=NONE  cterm=NONE
hi PreProc        ctermfg=04    ctermbg=NONE  cterm=NONE
hi Special        ctermfg=14    ctermbg=NONE  cterm=NONE
hi Statement      ctermfg=04    ctermbg=NONE  cterm=NONE
hi Type           ctermfg=05    ctermbg=NONE  cterm=NONE
hi Delimiter      ctermfg=08    ctermbg=NONE  cterm=NONE
hi Directory      ctermfg=04    ctermbg=NONE  cterm=NONE
hi Function       ctermfg=12    ctermbg=NONE  cterm=bold
hi Operator       ctermfg=08    ctermbg=NONE  cterm=NONE
hi SpecialComment ctermfg=15    ctermbg=NONE  cterm=bold
hi String         ctermfg=06    ctermbg=NONE  cterm=NONE
hi Title          ctermfg=15    ctermbg=NONE  cterm=NONE
hi Todo           ctermfg=15    ctermbg=NONE  cterm=bold
hi Typedef        ctermfg=13    ctermbg=NONE  cterm=NONE
hi Underlined     ctermfg=12    ctermbg=NONE  cterm=underline

hi Search         ctermfg=04    ctermbg=NONE  cterm=inverse
hi IncSearch      ctermfg=07    ctermbg=NONE  cterm=inverse
hi MatchParen     ctermfg=01    ctermbg=NONE  cterm=bold
hi Visual         ctermfg=15    ctermbg=00    cterm=NONE
hi Folded         ctermfg=08    ctermbg=NONE  cterm=NONE
hi FoldColumn     ctermfg=00    ctermbg=NONE  cterm=NONE
hi DiffAdd        ctermfg=02    ctermbg=00    cterm=NONE
hi DiffChange     ctermfg=NONE  ctermbg=00    cterm=NONE
hi DiffDelete     ctermfg=09    ctermbg=00    cterm=NONE
hi DiffText       ctermfg=11    ctermbg=00    cterm=NONE
"
hi CursorLine     ctermfg=NONE  ctermbg=00    cterm=NONE
hi CursorLineNr   ctermfg=08    ctermbg=00    cterm=NONE
hi ColorColumn    ctermfg=NONE  ctermbg=00    cterm=NONE
hi CursorColumn   ctermfg=NONE  ctermbg=00    cterm=NONE
hi LineNr         ctermfg=00    ctermbg=NONE  cterm=NONE
hi SignColumn     ctermfg=00    ctermbg=NONE  cterm=NONE
hi VertSplit      ctermfg=00    ctermbg=NONE  cterm=NONE
hi StatusLine     ctermfg=08    ctermbg=00    cterm=NONE
hi StatusLineNC   ctermfg=08    ctermbg=NONE  cterm=underline
hi WildMenu       ctermfg=07    ctermbg=NONE  cterm=inverse

hi Pmenu          ctermfg=08    ctermbg=00    cterm=NONE
hi PmenuSbar      ctermfg=00    ctermbg=00    cterm=NONE
hi PmenuSel       ctermfg=15    ctermbg=00    cterm=NONE
hi PmenuThumb     ctermfg=00    ctermbg=00    cterm=NONE

hi ErrorMsg       ctermfg=09    ctermbg=NONE  cterm=NONE
hi ModeMsg        ctermfg=08    ctermbg=NONE  cterm=NONE
hi MoreMsg        ctermfg=12    ctermbg=NONE  cterm=NONE
hi Question       ctermfg=12    ctermbg=NONE  cterm=NONE
hi WarningMsg     ctermfg=11    ctermbg=NONE  cterm=NONE
hi QuickFixLine   ctermfg=NONE  ctermbg=00    cterm=NONE
hi vimOption      ctermfg=12    ctermbg=NONE  cterm=NONE
hi SpellBad       ctermfg=01    ctermbg=NONE  cterm=undercurl
hi SpellCap       ctermfg=01    ctermbg=NONE  cterm=undercurl
hi SpellLocal     ctermfg=01    ctermbg=NONE  cterm=undercurl
hi SpellRare      ctermfg=01    ctermbg=NONE  cterm=undercurl

hi! link Terminal Normal
hi! link TabLine StatusLineNC
hi! link TabLineFill StatusLineNC
hi! link TabLineSel StatusLine
hi! link StatusLineTerm StatusLine
hi! link StatusLineTermNC StatusLineNC
hi! link VisualNOS Visual
hi! link diffAdded DiffAdd
hi! link diffBDiffer WarningMsg
hi! link diffChanged DiffChange
hi! link diffCommon WarningMsg
hi! link diffDiffer WarningMsg
hi! link diffFile Directory
hi! link diffIdentical WarningMsg
hi! link diffIndexLine Number
hi! link diffIsA WarningMsg
hi! link diffNoEOL WarningMsg
hi! link diffOnly WarningMsg
hi! link diffRemoved DiffDelete
hi! link Boolean Constant
hi! link Character Constant
hi! link Float Constant
hi! link Number Constant
hi! link StorageClass Statement
hi! link Conditional Statement
hi! link Exception Statement
hi! link Keyword Statement
hi! link Label Statement
hi! link Repeat Statement
hi! link Define PreProc
hi! link Include PreProc
hi! link Macro PreProc
hi! link PreCondit PreProc
hi! link Structure Type
hi! link Typedef Type
hi! link Debug Special
hi! link SpecialChar Special
hi! link Tag Special
hi! link Noise Delimiter
hi! link StringDelimiter Delimiter
hi! link Conceal Ignore
hi! link NonText Ignore
hi! link SpecialKey Ignore
hi! link EndOfBuffer Ignore
hi! link Whitespace Ignore
hi! link Identifier None
hi! link Searchlight IncSearch
hi! link cssBraces Delimiter
hi! link gitcommitOverflow Error
hi! link gitcommitSummary Title
hi! link helpHyperTextJump Underlined
hi! link htmlArg Function
hi! link htmlEndTag Delimiter
hi! link htmlLink Underlined
hi! link htmlSpecialTagName htmlTagName
hi! link htmlTag Delimiter
hi! link htmlTagName Statement
hi! link rustAttribute Delimiter
hi! link rustDerive rustAttribute
hi! link rustDeriveTrait rustDerive
hi! link rustIdentifier Typedef
hi! link rustModPath None
hi! link rustSigil Delimiter
hi! link scssAttribute Delimiter
hi! link vimAutoCmdSfxList Type
hi! link vimAutoEventList Identifier
hi! link vimCmdSep Special
hi! link vimCommentTitle SpecialComment
hi! link vimContinue Delimiter
hi! link vimHighlight Statement
hi! link vimUserFunc Function
